import pandas as pd

# calculates MCO and adds to dataframe
# expects columns 'Date', 'Adv', 'Dec' in dataframe
def mco(df, adjusted=True, period1=19, period2=39):
    # sort (date, ascending)
    df.sort_index(inplace=True)

    # calculate (Adjusted) Net Advances and the EMAs
    if adjusted:
        df['ANA'] = ( df['Adv'] - df['Dec'] ) / ( df['Adv'] + df['Dec'] )
    else:
        df['ANA'] = df['Adv'] - df['Dec']

    df['EMA fast'] = df['ANA'].ewm(span=period1, min_periods=0, adjust=False, ignore_na=False).mean()
    df['EMA slow'] = df['ANA'].ewm(span=period2, min_periods=0, adjust=False, ignore_na=False).mean()
    df['MCO'] = df['EMA fast'] - df['EMA slow']
    
    return df

# calculates MSCI and adds to dataframe
# expects columns 'Date', 'Adv', 'Dec' in dataframe
def msci(df, adjusted=True, period1=19, period2=39):
    # add MCO to dataframe
    df = mco(df, adjusted, period1, period2)
    
    # initialise MCSI series & first value
    df['MCSI'] = pd.Series()
    df['MCSI'][0] = 0

    # have to use a for, for this
    for i in range(1, len(df)):
        # MCSI today = MCSI yesterday + MCO today
        df['MCSI'][i] = df['MCSI'][i-1] + df['MCO'][i]
        
    return df
